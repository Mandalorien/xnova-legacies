<?php return array(
    'global' => array(
        'database' => array(
            'engine' => 'mysql',
            'options' => array(
                'hostname' => 'localhost',
                'username' => 'root',
                'password' => '',
                'database' => 'db_xnova'
                ),
            'table_prefix' => 'game_',
            )
        )
    );